all: compile

compile:
	ant -buildfile fileVisitors/src/build.xml all

run:
	ant -buildfile fileVisitors/src/build.xml run -Dargs='src/input.txt src/output.txt 1'

test:	
	ant -buildfile fileVisitors/src/build.xml test

clean: 	
	ant -buildfile fileVisitors/src/build.xml clean

